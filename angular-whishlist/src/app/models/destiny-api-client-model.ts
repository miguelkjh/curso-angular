import { DestinyTravel } from "../models/destiny-travel.model";
import { Subject, BehaviorSubject } from 'rxjs';

export class DestinyApiClient {
    destinos:DestinyTravel[];
    current: Subject<DestinyTravel> = new BehaviorSubject<DestinyTravel>(null);

	constructor() {
       this.destinos = [];
	}
	add(d:DestinyTravel){
	  this.destinos.push(d);
	}
	getAll(): DestinyTravel[]{
	  return this.destinos;
    }
    getById(id: string): DestinyTravel{
        return this.destinos.filter(function(d){return d.name == id;})[0]
    }
    change(d: DestinyTravel){
        this.destinos.forEach(x => x.setSelected(false));
        d.setSelected(true);
        this.current.next(d);
    }
    subscribeOnChange(fn){
        this.current.subscribe(fn);
    }
}