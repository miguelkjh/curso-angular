export class DestinyTravel {
    private selected: boolean;
    public services: string[];
    constructor(public name: string, public url: string) {
        this.services = ['pileta', 'desayuno']
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(flat: boolean) {
        this.selected = flat;
    }
}