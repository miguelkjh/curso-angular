import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinyTravel } from '../models/destiny-travel.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';


@Component({
  selector: 'app-form-destination-travel',
  templateUrl: './form-destination-travel.component.html',
  styleUrls: ['./form-destination-travel.component.css']
})
export class FormDestinationTravelComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinyTravel>;
  fg: FormGroup;
  readonly minLog: number = 6;
  searchResults: string[];
  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nameValidatorParam(this.minLog)
      ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form:any) => {
      console.log(form);
    })
  }

  save(name: string, url: string): boolean {
    let destiny = new DestinyTravel(name,url);
    this.onItemAdded.emit(destiny);
    return false;
  }

  nameValidator(control: FormControl): {[s: string]: boolean} {
    let l = control.value.toString().trim().length;
    if ( l > 0 && l < 5) {
      return { 
        invalidName: true,
        minLogName: true
      
      }
    }
    return null;
  }

  nameValidatorParam(min: number): ValidatorFn {
    return (control: FormControl): {[s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if ( l > 0 && l < min) {
        return { 
          invalidName: true,
          minLogName: true
        
        }
      }
      return null;
    }
  }

  ngOnInit(): void {
    const elemName = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemName, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter((text => text.length > 2 )),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('/assets/datos.json'))
    ).subscribe(ajaxResponse => {
      this.searchResults = ajaxResponse.response;
    })
  }

}
