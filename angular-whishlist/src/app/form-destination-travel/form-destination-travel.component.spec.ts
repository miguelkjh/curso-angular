import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDestinationTravelComponent } from './form-destination-travel.component';

describe('FormDestinationTravelComponent', () => {
  let component: FormDestinationTravelComponent;
  let fixture: ComponentFixture<FormDestinationTravelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDestinationTravelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDestinationTravelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
