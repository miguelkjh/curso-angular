import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinyTravel } from "../models/destiny-travel.model";
import { DestinyApiClient } from "../models/destiny-api-client-model";
import { Store, State } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destiny-list',
  templateUrl: './destiny-list.component.html',
  styleUrls: ['./destiny-list.component.css']
})
export class DestinyListComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinyTravel>;
  updates: string[];

  places: DestinyTravel[];

  constructor(public destinyApiClient: DestinyApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.place.favorito)
    .subscribe(data => {
      if (data != null) {
        this.updates.push('Se ha elegido a ' + data.name)
      }
    });
  }
  
  save(place: DestinyTravel) {
    this.destinyApiClient.add(place);
    this.onItemAdded.emit(place);
    this.store.dispatch(new NuevoDestinoAction(place));

  }

  choosen(place: DestinyTravel) {
    this.destinyApiClient.change(place);
    this.store.dispatch(new ElegidoFavoritoAction(place));
  }

  ngOnInit(): void {
  }

}
