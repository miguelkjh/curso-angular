import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { StoreModule as NgRxStoreModule, ActionReducerMap } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { StoreDevtools, StoreDevtoolsModule } from "@ngrx/store-devtools";

import { AppComponent } from './app.component';
import { DestinyTravelComponent } from './destiny-travel/destiny-travel.component';
import { DestinyListComponent } from './destiny-list/destiny-list.component';
import { Routes, RouterModule } from '@angular/router';
import { DestinationDetailsComponent } from './destination-details/destination-details.component';
import { FormDestinationTravelComponent } from './form-destination-travel/form-destination-travel.component';
import { DestinyApiClient } from './models/destiny-api-client-model';
import { DestinosViajesState, reducerDestinosViajes, intializeDestinosViajesState, DestinosViajesEffects } from './models/destinos-viajes-state.model';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: DestinyListComponent},
  {path: 'destination', component: DestinationDetailsComponent}
];

// redux init
export interface AppState{
  place: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  place: reducerDestinosViajes
};

const reducersInitialState = {
  place: intializeDestinosViajesState()
}

// redux fin init


@NgModule({
  declarations: [
    AppComponent,
    DestinyTravelComponent,
    DestinyListComponent,
    DestinationDetailsComponent,
    FormDestinationTravelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    DestinyApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
